# e-commerce-app

E-commerce app implemented using MERN technologies. 
Functionalities:

- admin panel
- shopping cart
- product search
- token based authentification
- order summary
- payment (paypal and credit card)