const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        max: 255,
        min: 6
    },
    password: {
        type: String, 
        required:true,
        max: 1024,
        min: 6
    }, 
    lastName: {
        type: String, 
        required: true
    },
    address: {
        type: String, 
        required: true
    },
    city: {
        type: String, 
        required: true
    },
    zip: {
        type: Number, 
        required: true
    },
    country: {
        type: String, 
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    isAdmin: {
        type: Boolean,
        required: true,
        default: false
    }
})

module.exports = mongoose.model("User", userSchema);


