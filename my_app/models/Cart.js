const mongoose = require("mongoose");

const CartItemSchema = new mongoose.Schema({
    id: { 
        type: String, 
        required: true },
    title: { 
        type: String, 
        required: true },
    quantity: { 
        type: Number, 
        required: true },
    price: { 
        type: String, 
        required: true },
    descr: {
      type: String
    }
});

const CartSchema = new mongoose.Schema({
    userId: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User'
    },
    products: [CartItemSchema],
    totalPrice: { type: Number } 
    },
    {
    timestamps: true
})

module.exports = mongoose.model("Cart", CartSchema);