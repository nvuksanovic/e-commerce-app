const mongoose = require('mongoose');
const dotenv = require('dotenv');
const express = require('express');
const cors = require('cors');
const GridFsStorage = require("multer-gridfs-storage");
const multer = require("multer");

//import routes
const authRoute = require('./routes/authentificationRoute');
const orderRoute = require('./routes/orderRoute');
const paymentRoute = require('./routes/paymentRoute');
const productRoute = require('./routes/productRoute');
const imageRoute = require('./routes/imageRoute');


const app = express();
app.use(cors());

dotenv.config();

//connect to db

const mongoURI = process.env.DATA_BASE_CONNECT;

mongoose.connect(process.env.DATA_BASE_CONNECT, 
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
  }).catch(error => console.log(error.reason));


// Create storage engine

const storage = new GridFsStorage({
  url: mongoURI,
  file: (req, file) => {
    return new Promise((resolve, reject) => {

        const fileInfo = {
          filename: file.originalname,
          bucketName: 'uploads'
        };
        resolve(fileInfo);
     
    });
  }
});

const upload = multer({ storage });
app.use(express.json()); 

//route middlewares

app.use('/api/user', authRoute); 
app.use('/api/orders', orderRoute);
app.use('/api/payment', paymentRoute);
app.use('/api/products', productRoute);
app.use('/api/images', imageRoute(upload));

app.listen(5000, () => console.log(`Listening on port`));


