const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
    const bearerToken = req.headers.authorization;
    const token = bearerToken.slice(7, bearerToken.length);

    if(!token) return res.status(401).send('Access Denied');

    try {
        const verified = jwt.verify(token, process.env.TOKEN_SECRET);
        console.log("VERIFIED ", verified)
        req.user = verified;
        next();
    } catch (error) {
        res.status(400).send('Invalid Token');
    }
}