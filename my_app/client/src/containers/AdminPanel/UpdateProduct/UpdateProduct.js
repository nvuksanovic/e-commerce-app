import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import classes from './UpdateProduct.css';
import styles from './UpdateProduct.css';
import EdiText from 'react-editext';

class UpdateProduct extends Component {

    

    constructor(props) {
        super(props);
        // Don't call this.setState() here!
        let product = this.props.products.find(prod => prod._id === this.props.match.params.id);
        this.state = {
            title: product.title,
            description: product.description,
            price: product.price,
            image: product.image,
            selectedProduct: product,
            uploading: false,
            file: null,
        }
        
    }
/*
    componentWillMount(){
        
        let product = this.props.products.find(prod => prod._id === this.props.match.params.id);
        this.setState({
            title: product.title,
            description: product.description,
            price: product.price,
            image: product.image,
            selectedProduct: product
        });
    }
*/
    getInputTitle = (val) => {
        this.setState({title: val});
    }

    getInputDescription = (val) => {
        this.setState({description: val});
    }

    getInputPrice = (val) => {
        this.setState({price: val});
    }

    fileSelectedHandler = (e) => {

        this.setState({image: e.target.files[0]});  
        this.setState({
            file: URL.createObjectURL(e.target.files[0])
          })
       
    }


    submitHandler = (e) => {
        e.preventDefault();

        //POST IMAGE TO SERVER 
        if(this.state.file !== null) {
            const bodyFormData = new FormData();
            bodyFormData.append('image', this.state.image);
            const url = 'http://localhost:5000/api/images/upload';
                    
            this.setState({ uploading: true });
            
            axios.post(url, bodyFormData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'x-Trigger': 'CORS'
                }
            })
            .then((response) => {
                this.setState({ uploading: false });     
                
                this.props.onUpdateProduct(
                    this.props.match.params.id,
                    this.state.title, 
                    this.state.description,
                    this.state.price,
                    this.state.image.name,
                    this.props.token
                )
                if(this.props.doneUpadatingProduct) 
                    this.props.history.push('/')
                
            })
            .catch((err) => {
                console.log(err);
            });
            
        }
        
        else {

            this.props.onUpdateProduct(
                this.props.match.params.id,
                this.state.title, 
                this.state.description,
                this.state.price,
                this.state.image,
                this.props.token
            )

            if(this.props.doneUpadatingProduct) 
                 this.props.history.push('/')
        }      
        
        
    }


    render() {
        
        return(
            
            <div className={classes.UpdateProduct}>
                <div>
                    <h1>Update Product</h1>
                    
                    <div id={styles.label}><label>Title</label></div>
                    <EdiText
                        type='textarea'
                        inputProps={{
                            className: 'textarea',
                            placeholder: 'Type your content here',
                            style: {
                            outline: 'none',
                            minWidth: 'auto'
                            },
                            rows: 1
                        }}
                        value={this.state.selectedProduct.title}
                        onSave={this.getInputTitle}
                    />
                        
                    <div id={styles.label}><label>Description</label></div>
                    <EdiText
                        type='textarea'
                        inputProps={{
                            className: 'textarea',
                            placeholder: 'Type your content here',
                            style: {
                            outline: 'none',
                            minWidth: 'auto'
                            },
                            rows: 5
                        }}
                        value={this.state.selectedProduct.description}
                        onSave={this.getInputDescription}
                    />
                    <div id={styles.label}><label>Price</label></div>
                    <EdiText
                        type='textarea'
                        inputProps={{
                            className: 'textarea',
                            placeholder: 'Type your content here',
                            style: {
                            outline: 'none',
                            minWidth: 'auto'
                            },
                            rows: 1
                        }}
                        value={this.state.selectedProduct.price}
                        onSave={this.getInputPrice}
                    />
                        
                    <div id={styles.label}><label>Image</label></div>
                    <input id={styles.file} type="file" onChange={(event) => this.fileSelectedHandler(event)}></input>

                    <div ><img src={this.state.file} id={styles.filePreview}></img></div>
                    <button id={styles.submit} onClick={(event) => this.submitHandler(event)}>
                        Update Product
                    </button>
                    
                </div>

            </div>
                
        )
    }
}

const mapStateToProps = state => {
    return {
        
        token: state.auth.token,
        products: state.red.products,
        doneUpadatingProduct: state.red.doneUpadatingProduct
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUpdateProduct : (id, title, description, price, image, token) => dispatch(actions.updateProduct(id, title, description, price, image, token)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(UpdateProduct));
