import React, { Component } from 'react';
import { connect } from 'react-redux';
import classes from './CreateProduct.css';
import styles from './CreateProduct.css';
import * as actions from '../../../store/actions/index';
import axios from 'axios';

class CreateProduct extends Component {

    state = {
        title: '',
        description: '',
        price: 0,
        image: '',
        loading: false,
        productToDelete: '',
        file: null
    }

    getInputTitle = (e) => {
        this.setState({title: e.target.value});
    }

    getInputDescription = (e) => {
        this.setState({description: e.target.value});
    }

    getInputPrice = (e) => {
        this.setState({price: e.target.value + '$'});
    }

    fileSelectedHandler = (e) => {

        this.setState({image: e.target.files[0]});  
        this.setState({
            file: URL.createObjectURL(e.target.files[0])
        })
        /*const getUrl = 'http://localhost:5000/api/images/'+ this.state.image.name;

        axios.get(getUrl, {
             headers: {
             'Content-Type': 'multipart/form-data',
             'x-Trigger': 'CORS'
             },
         })
         .then((response) => {
             this.setState({
                 imageSRC: getUrl
             })
         })
         .catch((err) => {
             console.log(err);
         });*/
       
    }


    submitHandler = (e) => {
        e.preventDefault();

        //POST IMAGE TO SERVER 
        
        const bodyFormData = new FormData();
        bodyFormData.append('image', this.state.image);
        const url = 'http://localhost:5000/api/images/upload';
                 
        this.setState({loading: true});
         
        axios.post(url, bodyFormData, {
        headers: {
            'Content-Type': 'multipart/form-data',
            'x-Trigger': 'CORS'
        }
        })
        .then((response) => {
            this.setState({loading: false});     
        })
        .catch((err) => {
            console.log(err);
        });

        this.props.onCreateProduct(
            this.state.title, 
            this.state.description,
            this.state.price,
            this.state.image.name,
            this.props.token
        )

        if(this.state.loading === false)
            this.props.history.push("/");
                
    }

    getProductToDelete = (e) => {
        this.setState({productToDelete: e.target.value});
    }

    deleteProductHandler  = (e) => {
        e.preventDefault();
        this.props.onDeleteProduct(this.state.productToDelete, this.props.token);
    }

    render() {
        return(
            <div className={classes.CreateProduct}>
                <div id={styles.createProduct}>
                    <h1>Create Product</h1>
                    
                    <label>Title</label>
                    <input type="text" onChange={(event) => this.getInputTitle(event)}></input>
                        
                    <label>Description</label>
                    <textarea  onChange={(event) => this.getInputDescription(event)} rows="4" cols="50"></textarea >
                        
                    <label>Price</label>
                    <input type="text" onChange={(event) => this.getInputPrice(event)}></input>
                        
                    <label>Image</label>
                    <input id={styles.file} type="file" onChange={(event) => this.fileSelectedHandler(event)}></input>
                
                    {this.state.loading ? <h1>Loading...</h1> : null}
                    <div ><img src={this.state.file} id={styles.filePreview}></img></div>
                    <button id={styles.submit} onClick={(event) => this.submitHandler(event)}>
                        Create Product
                    </button>
                
                </div>
            </div>
                
        )
    }
}

const mapStateToProps = state => {
    return {
        
        token: state.auth.token,
        doneDeletingProduct: state.red.doneDeletingProduct
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onCreateProduct : (title, description, price, image, token) => dispatch(actions.createProduct(title, description, price, image, token)),
        onDeleteProduct : (title, token) => dispatch(actions.deleteProduct(title, token))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateProduct);