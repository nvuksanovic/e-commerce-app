import React, { Component } from 'react';
import { connect } from 'react-redux';
import classes from './AdminPanel.css';
import styles from './AdminPanel.css';
import * as actions from '../../store/actions/index';
import { NavLink } from 'react-router-dom';
import DeleteDialog from '../../components/DeleteDialog/DeleteDialog';


class AdminPanel extends Component {

    state = {
        done: false
    }
    
    componentDidMount () {
        this.props.onFetchData();
    }


    deleteProductHandler  = (e, title) => {
        
        e.preventDefault();
        this.props.onDeleteProduct(title, this.props.token);
        
    }

    updateProductHandler = (e, id) => {
        
        e.preventDefault();
        
        this.props.history.push('/admin-panel/update/' + id);
    }

    render() {

        return(
            <div className={classes.AdminPanel}>
                <ul id={styles.products}>
                    {this.props.products.map(product => (
                        
                        <div id={styles.product} key={product._id}>
                            <div id={styles.img}><img src={'http://localhost:5000/api/images/'+ product.image} alt={product.alt} id={styles.item}/></div>
                            <div id={styles.title}><h1 id={styles.item}>{product.title}</h1></div>
                            <label id={styles.item}>{product.price}</label>
                            <h4 id={styles.item} onClick={(event) => this.updateProductHandler(event, product._id)}>Update</h4>
                            <h4 id={styles.item} onClick={(event) => this.deleteProductHandler(event, product.title)}>Delete</h4>
                        </div>
                    ))}
                    <div>
                        <NavLink to="/create-new-product"><button id={styles.submit}>Create new product</button></NavLink>
                    </div>
                </ul>
                {this.props.doneDeletingProduct ? <DeleteDialog /> : null}
            </div>
            
        )
    }
}

const mapStateToProps = state => {
    return {

        products: state.red.products,
        token: state.auth.token,
        doneDeletingProduct: state.red.doneDeletingProduct
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onDeleteProduct : (title, token) => dispatch(actions.deleteProduct(title, token)),
        onFetchData: () => dispatch(actions.fetchProductData())

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminPanel);