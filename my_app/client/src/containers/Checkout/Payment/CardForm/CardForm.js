import React, { Component } from 'react';
import { CardElement, injectStripe } from 'react-stripe-elements';
import classes from './CardForm.css';
import styles from './CardForm.css';
import { connect } from 'react-redux';
import axios from 'axios';
import * as actionTypes from '../../../../store/actions/actionTypes';
import Alert from '../../../../components/Alert/Alert';

class CardForm extends Component {

    state = {
        cardHolder: ''
    }

    inputHandler = (e) => {
        this.setState({cardHolder: e.target.value})
    }


    submitHandler = async e => {
        e.preventDefault();
       
       try {
        let { token } = await this.props.stripe.createToken({name: this.state.cardHolder});
        console.log(token)
        let totalPrice = this.props.grandTotal;
        let url = 'http://localhost:5000/api/payment';
        
        axios.post(url, { token, totalPrice }).then(response => {
            console.log(response);
            this.props.onPaid();

        }).catch(error => {
            const errorMessage = error.response.data;
            console.log(errorMessage);
        });

        //TODO: redirect, clear inputs, thank alert
       
       } catch (error) {
           throw error;
       }
    }

    render() {
        return ( 
            <main className={classes.CardForm}>
                    <div>
                        <div id={styles.productInfo}>
                            <h3>Total Price:</h3>
                            <h4>${this.props.grandTotal}</h4>
                            <label>Card Details</label>
                            <div id={styles.cardElement}>
                                <input type="text" placeholder="Card Holder" onChange={(event) => this.inputHandler(event)}></input>
                                <CardElement/>
                            </div>
                        </div>
                        <form >
                        <button onClick={(event) => this.submitHandler(event)}>
                            Buy Now
                        </button>
                        </form>
                    </div>
                    {
                        this.props.paid ? 
                            <Alert />: null
                    }
            </main>
        )
    }
    
}

const mapStateToProps = state => {
    return {
        grandTotal: state.cart.grandTotal,
        shoppingBag: state.cart.shoppingBag,
        paid: state.ord.paid
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onPaid: () => dispatch({type: actionTypes.IS_PAID})
    }   
}

export default connect(mapStateToProps, mapDispatchToProps)(injectStripe(CardForm));