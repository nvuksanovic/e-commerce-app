import React, { Component } from 'react';
import classes from './Payment.css';
import styles from './Payment.css';
import { connect } from 'react-redux';
import Paypal from '../../../components/PayPal/PayPal';
import { StripeProvider, Elements } from 'react-stripe-elements';
import InjectedCheckoutForm from './CardForm/CardForm';
import payPal from '../../../assets/payPal.png';
import creditCard from '../../../assets/creditCard.png';
import * as actions from '../../../store/actions/index';

class Payment extends Component {

    state = {
        openCreditCardDetails: false,
        openPaypal: false,
        shippingMethod: ''
    }

    componentDidMount() {

        this.props.onGetUserData(this.props.token);
        //this.props.onGetOrderData(this.props.token);

        switch (this.props.shipping) {
            case 0:
                this.setState({shippingMethod: '15 Days - FREE'}) 
                break;
            case 8:
                this.setState({shippingMethod: '2-5 Days - 8$'})
                break;
            case 15:
                this.setState({shippingMethod: '1 Day - 15$'})
                break;
            default:
                break;
        }
        
    }

    openCreditCardDetailsHandler = () => {
        this.setState({openCreditCardDetails: true, openPaypal: false});
    }

    closeCreditCardDetailsHandler = () => {
        this.setState({openPaypal: true, openCreditCardDetails: false});
    }

    render() {
        let creditCardDetails = null;
        if (this.state.openCreditCardDetails === true && this.state.openPaypal === false)
            creditCardDetails = (
                <StripeProvider apiKey="pk_test_51HSsZCILfzI5UxegQOPEkM3vwAiiJAXQBkDlqUg96RwDQ3gGbSgRUySjnSuxPQXGwRHcX9a26N5MVZD72se2hKV600A9qYi69W">
                    <Elements>
                        <InjectedCheckoutForm />
                    </Elements>
                </StripeProvider>
            )
        else if(this.state.openCreditCardDetails === false && this.state.openPaypal === true)
                creditCardDetails = <Paypal />
        

        let userInfo = null;

        if(this.props.userData !== null) {
            
            let products = this.props.shoppingCart.map(product => (
                    product.title
                )
            )

                userInfo = (
                    
                    <div id={styles.table}>
                        <h1>PLEASE CHECK YOUR INFORMATION</h1>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>FULL NAME</td>
                                        <td>{this.props.userData.name} {this.props.userData.lastName}</td>
                                    </tr>
                                    <tr>
                                        <td>EMAIL</td>
                                        <td>{this.props.userData.email}</td>
                                    </tr>
                                    <tr>
                                        <td>ADDRESS</td>
                                        <td>{this.props.userData.address}</td>
                                    </tr>
                                    <tr>
                                        <td>COUNTRY</td>
                                        <td>{this.props.userData.country}</td>
                                    </tr>
                                    <tr>
                                        <td>CITY</td>
                                        <td>{this.props.userData.city}</td>
                                    </tr>
                                    <tr>
                                        <td>ZIP/POSTCODE</td>
                                        <td>{this.props.userData.zip}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <table id={styles.orderInfo}>
                                <tbody>
                                    <tr>
                                        <td>ORDER ITEMS</td>
                                        <td>{products.join(' // ')}</td>
                                    </tr>
                                    <tr>
                                        <td>SHIPPING METHOD</td>
                                        <td>{this.state.shippingMethod}</td>
                                    </tr>
                                    <tr>
                                        <td>GRAND TOTAL</td>
                                        <td>{this.props.grandTotal}$</td>
                                    </tr>
                                </tbody>
                            </table>
                    </div>
                )    
        }
        
        return (    
            <div className={classes.Payment}>
               <div id={styles.info}>
                    {userInfo}
                    <div id={styles.paymentMethod}>
                        <h1>Please select your payment method</h1>
                        <div id={styles.method}>
                            <label>
                                <img src={payPal} alt="PayPal" onClick={this.closeCreditCardDetailsHandler}></img>
                            </label>
                                
                            <label>
                                <img src={creditCard} alt="CreditCard" onClick={this.openCreditCardDetailsHandler}></img>
                            </label>   
                        </div>
                        {creditCardDetails}
                    </div>             
               </div>
                        
            </div>
        )
        
    }
}

const mapStateToProps = state => {
    return {
        grandTotal: state.cart.grandTotal,
        token: state.auth.token,
        userData: state.auth.userData,
        loading: state.auth.loading,
        shipping: state.cart.shipping,
        orderData: state.ord.orderData,
        shoppingCart: state.cart.shoppingBag
    }
}

const mapDispatchToProps = dispatch => {
    return {

        onGetUserData: (token) => dispatch(actions.getUserData(token)),
        onGetOrderData: (token) => dispatch(actions.getOrderData(token))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Payment);