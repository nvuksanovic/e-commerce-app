import React, { Component } from 'react';
import classes from './SignIn.css';
import styles from './SignIn.css';
import { NavLink, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';


class SignIn extends Component {

    state = {
        email: '',
        password: ''
    }

    componentDidUpdate(){
        this.props.isLoggedIn ? (
            this.props.isAdmin === false ? 
                this.props.history.goBack() : 
                this.props.history.push("/admin-panel")
        )  : null
    }

    submitHandler = (e) => {
        e.preventDefault();
        this.props.onUserLoginRequest(this.state.email, this.state.password);
    }

    emailInputHandler = (e) => {
        this.setState({email: e.target.value});
    }

    passwordInputHandler = (e) => {
        this.setState({password: e.target.value});
    }

    render() {
        /*
        let redirect = '';
        this.props.location.pathname === "/checkout" || this.props.location.pathname === "/shopping-bag" ? redirect = "/payment" : redirect = "/";
        */
        return(
            <div className={classes.SignIn}>
                <h1>Sign in</h1>
                <form>
                    <input type="email" placeholder="Email" onChange={(event) => this.emailInputHandler(event)}></input>
                    <input type="password" placeholder="Password" onChange={(event) => this.passwordInputHandler(event)}></input>
                    <div id={styles.error}>{<p>{this.props.error}</p>}</div>
                    <button id={styles.submit} onClick={(event) => this.submitHandler(event)}>
                        <span></span>
                        <span></span>
                        Submit
                    </button>
                </form>
                <div id={styles.register}><NavLink to="/register">New to Glossier? Create account here!</NavLink></div>
            </div>
        )
    }
}


const mapStateToProps = state => {
    return {
        email: state.auth.email,
        error: state.auth.signInError,
        isLoggedIn: state.auth.isLoggedIn,
        isAdmin: state.auth.isAdmin
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUserLoginRequest: (email, password) => dispatch(actions.userLoginRequest(email, password)),
        onUserLogOut: () => dispatch(actions.userLogOut())
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SignIn));