import React from 'react';
import classes from './ShoppingCartItem.css';
import styles from './ShoppingCartItem.css';
import Quantitiy from '../../../components/UI/Quantity/Quantity';

const shoppingCartItem = (props) => {


    return (
        <div className={classes.ShoppingCartItem}>
            <div id={styles.img}><img src={'http://localhost:5000/api/images/'+ props.src} alt={props.alt} id={styles.item}/></div>
            <div id={styles.title}><h1 id={styles.item}>{props.title}</h1></div>
            <label id={styles.item}>{props.price} X {props.quantity}</label>
            <Quantitiy id={props.id} quantity={props.quantity} productSpec={ false } /> 
            <h4 id={styles.item} onClick={props.remove}>Remove</h4>
        </div>
        
    )
}

export default shoppingCartItem;