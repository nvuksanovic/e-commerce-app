import React, { Component } from 'react';
import classes from './ShoppingCart.css';
import styles from './ShoppingCart.css';
import ShoppingCartItem from './ShoppingCartItem/ShoppingCartItem';
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions/actionTypes';
import * as actions from '../../store/actions/index';
import { withRouter } from 'react-router-dom';

class ShoppingCart extends Component {

    removeItemHandler = (id) => {

        this.props.onRemoveFromCart(id);
    }

    checkoutButtonHandler = (e) => {
        e.preventDefault();
        if(this.props.isLoggedIn)
        {
            this.props.onPostOrderData(this.props.shoppingBag, this.props.grandTotal, this.props.token);
            this.props.history.push("/payment");
        }
        else {
            this.props.history.push("/sign-in");
        }
    }

    render() {

        let cart = <h1 style={{
            fontFamily: 'Montserrat', 
            fontSize: '20px', 
            textTransform: 'uppercase', 
            fontWeight: 'normal', 
            padding: '40px'}}>Your shopping bag is empty!</h1>

        if(this.props.shoppingBag.length !== 0) {
            cart = (
                <div className={classes.ShoppingCart}>
                    <div id={styles.productsList}>
                        <ul>
                            {this.props.shoppingBag.map(item => (
                                <ShoppingCartItem 
                                    key={item.id}
                                    id={item.id}
                                    title={item.title}
                                    quantity={item.quantity}
                                    price={item.price}
                                    src={this.props.products.find(product => product._id === item.id).image}
                                    alt="product"
                                    remove={() => this.removeItemHandler(item.id)}
                                />
                            ))}
                        </ul>
                    </div>
                    <div id={styles.checkout}>
                        <label>Subtotal: ${this.props.totalPrice}</label> 
                        <div id={styles.shipping}>
                        <label>Shipping: </label>
                            <label>
                                <input type="radio" name="options" onClick={() => this.props.onCalculateGrandTotal(15)}/>
                                <span></span>
                                <span>1 Day  15$</span>
                            </label>
                            <label>
                                <input type="radio" name="options" onClick={() => this.props.onCalculateGrandTotal(8)}/>
                                <span></span>
                                <span>2-5 Days  8$</span>
                            </label>
                            <label>
                                <input type="radio" name="options"  defaultChecked onClick={() => this.props.onCalculateGrandTotal(0)}/>
                                <span></span>
                                <span>15 Days  FREE</span>
                            </label>
                        </div>
                        <hr/>
                            <label id={styles.grandTotal}>Grand total: ${this.props.grandTotal}</label>
                            <button id={styles.checkoutButton} onClick={(event) => this.checkoutButtonHandler(event)}>Checkout</button>
                    </div>
                </div>
            )
        }

        return (
            <div>
                {cart}
            </div>
        ) 
    }
}

const mapStateToProps = state => {
    return {
        shoppingBag: state.cart.shoppingBag,
        totalPrice: state.cart.totalPrice,
        products: state.red.products,
        grandTotal: state.cart.grandTotal,
        quantity: state.cart.quantity,
        token: state.auth.token,
        isLoggedIn: state.auth.isLoggedIn
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onRemoveFromCart: (id) => dispatch({type: actionTypes.REMOVE_FROM_CART, id: id}),
        onCalculateGrandTotal: (shipping) => dispatch({type: actionTypes.CALCULATE_GRAND_TOTAL, shipping: shipping}),
        onAddToCart: (id, price, title, descr) => dispatch({type: actionTypes.ADD_TO_CART_WITH_QTY_BTN, id: id, price: price, title: title, descr: descr}),
        onPostOrderData: (orderData, grandTotal, token) => dispatch(actions.postOrderData( orderData, grandTotal, token))
    } 
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ShoppingCart));