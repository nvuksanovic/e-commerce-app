import React, { Component } from 'react';
import NavigationBar from '../../components/Navigation/NavigationBar/NavigationBar';
import Products from '../Products/Products';
import ProductDetails from '../ProductDetails/ProductDetails';
import {Route, Switch} from 'react-router-dom';
import ShoppingCart from '../ShoppingCart/ShoppingCart';
import Checkout from '../Checkout/Checkout';
import Register from '../Checkout/Register/Register';
import Payment from '../../containers/Checkout/Payment/Payment';
import SignIn from '../Checkout/SignIn/SignIn';
import AdminPanel from '../AdminPanel/AdminPanel';
import SignOut from '../../components/SignOut/SignOut';
import SearchProducts from '../Products/SearchProducts';
import CreateProduct from '../AdminPanel/CreateProduct/CreateProduct';
import UpdateProduct from '../AdminPanel/UpdateProduct/UpdateProduct';
import GuardedRoute from '../../hoc/GuardedRoute/GuardedRoute';
import { connect } from 'react-redux';

class Store extends Component {

    render() {
        return (
            <div>
                <NavigationBar />
                <Switch>
                    <GuardedRoute path="/create-new-product"  component={CreateProduct} auth={this.props.isAdmin && this.props.isLoggedIn} /> 
                    <GuardedRoute path="/admin-panel/update/:id" exact component={UpdateProduct} auth={this.props.isAdmin && this.props.isLoggedIn} />   
                    <Route path="/search"  component={SearchProducts} />
                    <Route path="/product-details/:id" exact component={ProductDetails} />
                    <GuardedRoute path="/admin-panel" exact component={AdminPanel} auth={this.props.isAdmin && this.props.isLoggedIn} />
                    <Route path="/shopping-bag" exact component={ShoppingCart}></Route>
                    <Route path="/register" exact component={Register} />
                    <Route path="/checkout" component={Checkout}></Route>
                    <Route path="/sign-in" exact component={SignIn}></Route>
                    <Route path="/sign-out" exact component={SignOut}></Route>
                    <Route path="/payment" exact component={Payment}></Route>
                    <Route path="/" exact component={Products}></Route>
                </Switch>
            </div>
        )
    }
}


const mapStateToProps = state => {
    return {
        isLoggedIn: state.auth.isLoggedIn,
        isAdmin: state.auth.isAdmin
    }
}

export default connect(mapStateToProps, null)(Store);


