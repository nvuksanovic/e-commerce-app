import React, { Component } from 'react';
import { connect } from 'react-redux';
import FullProduct from '../../components/FullProduct/FullProduct';

class ProductDetail extends Component {


    render() {
        
        return (
            <div>
                {this.props.products.map(product => (
                    product._id === this.props.selectedId ?
                        <FullProduct 
                            key={product._id}
                            id={product._id}
                            src={product.image}
                            title={product.title}
                            description={product.description}
                            price={product.price}
                        /> : null
                ))}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        products: state.red.products,
        selectedId: state.red.selectedId,
        shoppingBag: state.cart.shoppingBag
    }
}

export default connect(mapStateToProps)(ProductDetail);