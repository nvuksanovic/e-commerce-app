import React, { Component } from 'react';
import Product from '../../components/Product/Product';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import * as actionTypes from '../../store/actions/actionTypes';
import {  Grid, Row, Col } from 'react-flexbox-grid';
import nextId from "react-id-generator";

class Products extends Component { 


    componentDidMount () {

        this.props.onFetchData();
    }
    
    productSelectedHandler = (id) => {

        this.props.onSetSelectedId(id);
        this.props.history.push('/product-details/' + id);

    }

    addToCartHandler = (id, price, title, descr) => {       
        this.props.onAddToCart(id, price, title, descr);     
    }

    arrayChunk = (arr, size) => {
        if (!Array.isArray(arr)) {
          throw new TypeError('Input should be Array');
        }
      
        if (typeof size !== 'number') {
          throw new TypeError('Size should be a Number');
        }
      
        var result = [];
        for (var i = 0; i < arr.length; i += size) {
          result.push(arr.slice(i, size + i));
        }
      
        return result;
      };

    render() {

        const rows = this.arrayChunk(this.props.products, 3)
        let products = rows.map(row => (
            <Row key={nextId()}>
            {
                row.map(product => 
                (
                    <Col sm={12} md={4} key={nextId()}>
                        <Product 
                            key={product._id}
                            src={product.image} 
                            alt={product.title} 
                            price={product.price} 
                            title={product.title}
                            selected={() => this.productSelectedHandler(product._id)}
                            addToCart={() => this.addToCartHandler(
                                            product._id, 
                                            product.price,
                                            product.title,
                                            product.description)}
                        />
                    </Col>
              ))
            }
             </Row>
          ))
          


        return (
            <div>
                <Grid>
                    {products} 
                </Grid>
            </div>
        )
    }
    
}

const mapStateToProps = state => {
    return {
        products: state.red.products,
        searchProducts: state.red.searchProducts,
        selectedId: state.red.selectedId,
        shoppingBag: state.cart.shoppingBag
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchData: () => dispatch(actions.fetchProductData()),
        onSetSelectedId: (id) => dispatch({type: actionTypes.SET_SELECTED_ID, id: id}),
        onAddToCart: (id, price, title, descr) => dispatch({type: actionTypes.ADD_TO_CART, id: id, price: price, 
            title: title, descr: descr})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Products);

