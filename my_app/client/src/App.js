import React, { Component } from 'react';

import classes from './App.css';
import { BrowserRouter } from 'react-router-dom';
import Store from './containers/Store/Store';

class App extends Component {
 
  render() {
    

    return (
      <BrowserRouter>
        <div className={classes.App}>
          <Store />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
