import * as actionTypes from '../actions/actionTypes';

const initialState = {
    email: '',
    password: '',
    name: '',
    token: null,
    userId: null,
    loading: false,
    error: null,
    isLoggedIn: false,
    isAdmin: false,
    userData: null,
    signInError: '',
    registerError: ''
}

const reducer = (state=initialState, action) => {
    switch(action.type){
      
        case actionTypes.USER_REGISTER_SUCCESS:
            return {
                ...state,
                //userData: action.userData,
                loading: false,
                error: null,
                isLoggedIn: true,
                token: action.idToken
            }
        
        case actionTypes.USER_REGISTER_FAIL:
            return {
                ...state,
                registerError: action.error,
                isLoggedIn: false
            }
        
        case actionTypes.USER_REGISTER_REQUEST:
            return {
                ...state,
                loading: true,
                email: action.email,
                password: action.password,
                name: action.name
            }
            
            case actionTypes.USER_LOGIN_SUCCESS:
                return {
                    ...state,
                    loading: false,
                    isLoggedIn: true,
                    error: null,
                    token: action.idToken,
                    userId: action.userId,
                    isAdmin: action.isAdmin
                }
            
            case actionTypes.USER_LOGIN_FAIL:
                return {
                    ...state,
                    signInError: action.error,
                    isLoggedIn: false
                }
            
            case actionTypes.USER_LOGIN_REQUEST:
                return {
                    ...state,
                    loading: true,
                    email: action.email,
                    password: action.password
                }
            
            case actionTypes.USER_LOGOUT_SUCCESS:
                return {
                    ...state,
                    isLoggedIn: false
                }
            
            case actionTypes.GET_USER_DATA_FAIL:
                return {
                    ...state,
                    error: action.error
                }

            case actionTypes.GET_USER_DATA_SUCCESS:
                return {
                    ...state,
                    userData: action.userData,
                    loading: false
                }  
                
            case actionTypes.GET_USER_DATA_START:
                return {
                    ...state,
                    loading: true
                }       

        default:
            return state;
    }
}

export default reducer;