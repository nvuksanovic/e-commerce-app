import * as actionTypes from '../actions/actionTypes';

const initialState = {
    products: [],
    selectedId: null,
    totalPrice: 0,
    error: null,
    productData: null,
    searchProducts: [],
    searchKeyword: '',
    doneDeletingProduct: false,
    doneUpadatingProduct: false
}

const products = (state=initialState, action) => {
    switch(action.type){
      
        case actionTypes.FETCH_PRODUCT_SUCCESS:
            return {
                ...state,
                products: action.products,
                doneDeletingProduct: false
            }
        
        case actionTypes.SET_SELECTED_ID:
            return {
                ...state,
                selectedId: action.id,
    
            }

        case actionTypes.CREATE_PRODUCT_SUCCESS:
            return {
                ...state,
                productData: action.productData,
                doneDeletingProduct: false
            }
            
        case actionTypes.CREATE_PRODUCT_FAIL:
            return {
                ...state,
                error: action.error
            }
        
        case actionTypes.DELETE_PRODUCT_SUCCESS:
            return {
                ...state,
                doneDeletingProduct: true
            }

        case actionTypes.DELETE_PRODUCT_FAIL:
            return {
                ...state,
                doneDeletingProduct: false
            }
    
        case actionTypes.FETCH_PRODUCT_BY_SEARCH_KEYWORD_SUCCESS:
            return {
                ...state,
                searchProducts: action.products,
                searchKeyword: action.searchKeyword
            }

        case actionTypes.FETCH_PRODUCT_BY_SEARCH_KEYWORD_FAIL:
            return {
                ...state,
                error: action.error
            }

        case actionTypes.UPDATE_PRODUCT_FAIL:
            return {
                ...state,
                error: action.error,
                doneUpadatingProduct: false
                }
        case actionTypes.UPDATE_PRODUCT_SUCCESS:

            return {
                ...state,
                productData: action.productData,
                doneUpadatingProduct: true
        }

        default:
            return state;
    }
}

export default products;