import { stat } from 'fs';
import * as actionTypes from '../actions/actionTypes';

const initialState = {
    shoppingBag: [],
    quantity: 1,
    totalPrice: 0,
    grandTotal: 0,
    shipping: 0
}


const reducer = (state=initialState, action) => {
    switch(action.type){

        case actionTypes.RESET_SHOPPING_BAG:
            return {
                ...state,
                shoppingBag: [],
                quantity: 1,
                totalPrice: 0,
                grandTotal: 0,
                shipping: 0
            }

        case actionTypes.CALCULATE_GRAND_TOTAL:
            return {
                ...state,
                grandTotal: state.totalPrice + action.shipping,
                shipping: action.shipping
            }

        case actionTypes.INCREMENT_QUANTITY:

            let selectedProductToInc = state.shoppingBag.find(product => product.id === action.id);

            if(selectedProductToInc) {

                return {
                    ...state,
                    quantity: state.quantity + 1,
                    shoppingBag: state.shoppingBag.map(product =>
                        product.id === action.id
                          ? {...product, 
                            quantity: product.quantity + 1
                          }
                          : product 
                      ),
                    totalPrice: state.totalPrice + parseInt(selectedProductToInc.price, 10),
                    grandTotal: state.totalPrice + parseInt(selectedProductToInc.price, 10)
                       
                }
            } else {

                return {
                    ...state,
                    quantity: state.quantity + 1,
    
                }
            }
            
        
        case actionTypes.DECREMENT_QUANTITY:

            let selectedProductToDec = state.shoppingBag.find(product => product.id === action.id);
            
            if(selectedProductToDec) {
                return {
                    ...state,
                    quantity: state.quantity > 1 ? state.quantity - 1 : 1,
                    shoppingBag: state.shoppingBag.map(product =>
                        product.id === action.id
                          ? {...product, 
                            quantity: product.quantity > 1 ? product.quantity - 1 : 1
                          }
                          : product 
                      ),
                      totalPrice: state.totalPrice - parseInt(selectedProductToDec.price, 10),
                      grandTotal: state.totalPrice - parseInt(selectedProductToDec.price, 10) 
                      
                }
            }
            else {
                return {
                    ...state,
                    quantity: state.quantity - 1
                }
            }
            

        case actionTypes.RESET_QUANTITY:
            return {
                ...state,
                quantity: 1     
            }

        case actionTypes.REMOVE_FROM_CART:

            let selectedProduct =  state.shoppingBag.find(product => product.id === action.id);
            let updatedShoppingBag = state.shoppingBag.filter(product => product.id !== action.id)
            
            return {
                ...state,
                shoppingBag: updatedShoppingBag,
                totalPrice: state.totalPrice > 0 ? state.totalPrice - parseInt(selectedProduct.price, 10) * selectedProduct.quantity : 0,
                grandTotal: state.totalPrice > 0 ? state.totalPrice - parseInt(selectedProduct.price, 10) * selectedProduct.quantity : 0
            }
        
        case actionTypes.ADD_TO_CART:
        
            const exits = state.shoppingBag.find(product => product.id === action.id);

            if(exits) {
                return {
                    ...state,
                    shoppingBag: state.shoppingBag.map(product =>
                      product.id === action.id
                        ? {...product, quantity: product.quantity + 1, 
                            price: action.price,
                            title: action.title,
                            descr: action.descr
                        }
                        : product 
                    ),
                    totalPrice: state.totalPrice + parseInt(action.price, 10), 
                    grandTotal: state.totalPrice + parseInt(action.price, 10)
                }
            }
            else {
                return {
                        ...state,
                        shoppingBag: state.shoppingBag.concat({
                            id: action.id, 
                            quantity: state.quantity, 
                            price: action.price,
                            title: action.title,
                            descr: action.descr
                        }),
                        totalPrice: state.totalPrice + parseInt(action.price, 10) * state.quantity,
                        grandTotal: state.totalPrice + parseInt(action.price, 10) * state.quantity
                }
            }

        default:
            return state;
    }
}

export default reducer;