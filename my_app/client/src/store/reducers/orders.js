import * as actionTypes from '../actions/actionTypes';

const initialState = {
    orderData: null,
    orderID: null,
    error: null,
    paid: false
}

const reducer = (state=initialState, action) => {
    switch(action.type){

        case actionTypes.IS_PAID:
            return {
                ...state,
                paid: true
            }
        
        case actionTypes.POST_ORDER_DATA_SUCCESS:
            return {
                ...state,
                orderID: action.orderID
            }
      
        case actionTypes.GET_ORDER_DATA_SUCCESS:
            return {
                ...state,
                orderData: action.orderData
            }
        
        case actionTypes.GET_USER_DATA_FAIL:
            return {
                ...state,
                error: action.error
    
            }

        default:
            return state;
    }
}

export default reducer;