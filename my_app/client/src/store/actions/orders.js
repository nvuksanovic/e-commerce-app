import * as actionTypes from './actionTypes';
import axios from 'axios';


export const postOrderDataSuccess = (orderID) => {
    return {
        type: actionTypes.POST_ORDER_DATA_SUCCESS,
        orderID: orderID
    }
}


export const postOrderDataFail = (error) => {
    return {
        type: actionTypes.POST_ORDER_DATA_FAIL,
        error: error
    }
}


export const postOrderData = (orderData, grandTotal, token) => {
    return dispatch => {
     
        let url = 'http://localhost:5000/api/orders';
        
        axios.post(url, {orderItems: orderData, totalPrice: grandTotal},{
            headers: {
              Authorization: ' Bearer ' + token
            }}).then(response => {
            dispatch( postOrderDataSuccess(response.data) );
        }).catch(error => {
            const errorMessage = error.response.data;
            dispatch( postOrderDataFail(errorMessage) );
        });
    }
}

export const getOrderDataSuccess = (orderData) => {
    return {
        type: actionTypes.GET_ORDER_DATA_SUCCESS,
        orderData: orderData
    }
}


export const getOrderDataFail = (error) => {
    return {
        type: actionTypes.GET_ORDER_DATA_FAIL,
        error: error
    }
}


export const getOrderData = (token) => {
    return dispatch => {
     
        let url = 'http://localhost:5000/api/orders/' + token;
        
        axios.get(url,{
            headers: {
              Authorization: ' Bearer ' + token
            }}).then(response => {
            dispatch( getOrderDataSuccess(response.data) );
        }).catch(error => {
            const errorMessage = error.response.data;
            dispatch( getOrderDataFail(errorMessage) );
        });
    }
}