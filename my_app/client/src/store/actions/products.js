import * as actionTypes from './actionTypes';
import axios from 'axios';

export const fetchProductDataSuccess = (products) => {
    return {
        type: actionTypes.FETCH_PRODUCT_SUCCESS,
        products: products
    }
}


export const fetchProductDataFail = (error) => {
    return {
        type: actionTypes.FETCH_PRODUCT_FAIL,
        error: error
    }
}


export const fetchProductData = () => {
    return dispatch => {
        
        axios.get('/api/products/data').then(response => {
            dispatch( fetchProductDataSuccess(response.data) );
        }).catch(error => {
            const errorMessage = error.response.data;
            dispatch( fetchProductDataFail(errorMessage) );
        });;
    }
}

export const fetchProductsBySearchKeywordSuccess = (products, searchKeyword) => {
    return {
        type: actionTypes.FETCH_PRODUCT_BY_SEARCH_KEYWORD_SUCCESS,
        products: products,
        searchKeyword: searchKeyword
    }
}

export const fetchProductsBySearchKeywordFail  = (error) => {
    return {
        type: actionTypes.FETCH_PRODUCT_BY_SEARCH_KEYWORD_FAIL,
        error: error
    }
}

export const fetchProductSBySearchKeyword = (searchKeyword) => {
    return dispatch => {

        axios.get('http://localhost:5000/api/products/search?searchKeyword=' + searchKeyword).then(response => {
            dispatch( fetchProductsBySearchKeywordSuccess(response.data, searchKeyword) );
        }).catch(error => {
            const errorMessage = error.response.data;
            dispatch( fetchProductsBySearchKeywordFail(errorMessage) );
        });
    }
}

export const createProductSuccess = (productData) => {
    return {
        type: actionTypes.CREATE_PRODUCT_SUCCESS,
        productData: productData
    }
}

export const createProductFail = (error) => {
    return {
        type: actionTypes.CREATE_PRODUCT_FAIL,
        error: error
    }
}

export const createProduct = (title, description, price, image, token) => {
    return dispatch => {
        
        const productData = {
            title: title,
            description: description,
            price: price,
            image: image
        }
        
        let url = 'http://localhost:5000/api/products';
        
        axios.post(url, productData ,{
            headers: {
              Authorization: ' Bearer ' + token
            }}).then(response => {
            dispatch( createProductSuccess(productData) );
        }).catch(error => {
            const errorMessage = error.response.data;
            dispatch( createProductFail(errorMessage) );
        });
    }
}

export const deleteProductSuccess = () => {
    return {
        type: actionTypes.DELETE_PRODUCT_SUCCESS
    }
}

export const deleteProductFail = (error) => {
    return {
        type: actionTypes.DELETE_PRODUCT_FAIL,
        error: error
    }
}

export const deleteProduct = (title, token) => {
    return dispatch => {
        let url = 'http://localhost:5000/api/products/' + title;

        axios.delete(url, {
                headers: {
                Authorization: ' Bearer ' + token
            }}).then(response => {
            dispatch( deleteProductSuccess() );
        }).catch(error => {
            const errorMessage = error.response.data;
            dispatch( deleteProductFail(errorMessage) );
        });
    }
}

export const updateProductSuccess = (productData, id) => {
    return {
        type: actionTypes.UPDATE_PRODUCT_SUCCESS,
        productData: productData,
        id: id
    }
}

export const updateProductFail = (error) => {
    return {
        type: actionTypes.UPDATE_PRODUCT_FAIL,
        error: error
    }
}

export const updateProductStart = () => {
    return {
        type: actionTypes.UPDATE_PRODUCT_START
       
    }
}

export const updateProduct = (id, title, description, price, image, token) => {
    return dispatch => {
        
        dispatch( updateProductStart() );

        const productData = {
            title: title,
            description: description,
            price: price,
            image: image
        }
        
        let url = 'http://localhost:5000/api/products/update/' + id;
        
        axios.put(url, productData ,{
            headers: {
              Authorization: ' Bearer ' + token
            }}).then(response => {
            dispatch( updateProductSuccess(productData, id) );
        }).catch(error => {
            const errorMessage = error.response.data;
            dispatch( updateProductFail(errorMessage) );
        });
    }
}