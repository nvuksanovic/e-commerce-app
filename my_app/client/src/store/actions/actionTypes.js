export const FETCH_PRODUCT_DATA = 'FETCH_PRODUCT_DATA';
export const FETCH_PRODUCT_FAIL = 'FETCH_PRODUCT_FAIL';
export const FETCH_PRODUCT_SUCCESS = 'FETCH_PRODUCT_SUCCESS';
export const CREATE_PRODUCT_SUCCESS = 'CREATE_PRODUCT_SUCCESS';
export const CREATE_PRODUCT_FAIL = 'CREATE_PRODUCT_FAIL';
export const DELETE_PRODUCT_SUCCESS = 'DELETE_PRODUCT_SUCCESS';
export const DELETE_PRODUCT_FAIL = 'DELETE_PRODUCT_FAIL';
export const UPDATE_PRODUCT_START = 'UPDATE_PRODUCT_START';
export const UPDATE_PRODUCT_SUCCESS = 'UPDATE_PRODUCT_SUCCESS';
export const UPDATE_PRODUCT_FAIL = 'UPDATE_PRODUCT_FAIL';
export const FETCH_PRODUCT_BY_SEARCH_KEYWORD_FAIL = ' FETCH_PRODUCT_BY_SEARCH_KEYWORD_FAIL';
export const FETCH_PRODUCT_BY_SEARCH_KEYWORD_SUCCESS = 'FETCH_PRODUCT_BY_SEARCH_KEYWORD_SUCCESS';

export const SET_SELECTED_ID = 'SET_SELECTED_ID';
export const ADD_TO_CART = 'ADD_TO_CART';
export const ADD_TO_CART_WITH_QTY_BTN = 'ADD_TO_CART_WITH_QTY_BTN';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const INCREMENT_QUANTITY = 'INCREMENT_QUANTITY';
export const DECREMENT_QUANTITY = 'DECREMENT_QUANTITY';
export const RESET_QUANTITY = 'RESET_QUANTITY';
export const CALCULATE_GRAND_TOTAL = 'CALCULATE_GRAND_TOTAL';

export const USER_REGISTER_REQUEST = 'USER_REGISTER_REQUEST';
export const USER_REGISTER_SUCCESS = 'USER_REGISTER_SUCCESS';
export const USER_REGISTER_FAIL = 'USER_REGISTER_FAIL';

export const USER_LOGIN_REQUEST = 'USER_LOGIN_REQUEST';
export const USER_LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS';
export const USER_LOGIN_FAIL = 'USER_LOGIN_FAIL';
export const USER_LOGOUT_SUCCESS = 'USER_LOGOUT_SUCCESS';

export const POST_ORDER_DATA = 'POST_ORDER_DATA';
export const POST_ORDER_DATA_SUCCESS = 'POST_ORDER_DATA_SUCCESS';
export const POST_ORDER_DATA_FAIL = 'POST_ORDER_DATA_FAIL';

export const GET_ORDER_DATA = 'GET_ORDER_DATA';
export const GET_ORDER_DATA_SUCCESS = 'GET_ORDER_DATA_SUCCESS';
export const GET_ORDER_DATA_FAIL = 'GET_ORDER_DATA_FAIL';

export const GET_USER_DATA_START = 'GET_USER_DATA_START';
export const GET_USER_DATA_SUCCESS = 'GET_USER_DATA_SUCCESS';
export const GET_USER_DATA_FAIL = 'GET_USER_DATA_FAIL';

export const IS_PAID = 'IS_PAID';
export const RESET_SHOPPING_BAG = 'RESET_SHOPPING_BAG';
