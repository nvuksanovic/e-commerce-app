import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import './assets/fonts/Montserrat/Montserrat-Regular.ttf';
import './assets/fonts/Lato/Lato-Regular.ttf';
import {createStore, compose, applyMiddleware, combineReducers} from 'redux';
import products from './store/reducers/products';
import cartReducer from './store/reducers/cart';
import auth from './store/reducers/authentification';
import orders from './store/reducers/orders';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';

import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { PersistGate } from 'redux-persist/integration/react';

const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['red']
}

const rootReducer = combineReducers({
    cart: cartReducer,
    red: products,
    auth: auth,
    ord: orders
});


const persistedReducer = persistReducer(persistConfig, rootReducer);

const logger = store => {
    return next => {
        return action => {
            const result = next(action);
            return result;
        }
    }
};


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(persistedReducer, composeEnhancers(
    applyMiddleware(logger, thunk)
));

const persistor = persistStore(store);

ReactDOM.render(
    <Provider store={store}>
        <PersistGate persistor={persistor}>
            <App />
        </PersistGate>
    </Provider>, document.getElementById('root'));
registerServiceWorker();
