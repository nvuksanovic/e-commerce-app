import React, { Component } from 'react';
import classes from './ShoppingBagTracker.css';
import { connect } from 'react-redux';

class ShoppingBagTracker extends Component {

    render() {
        return (
            <div className={classes.ShoppingBagTracker}>
               <span id="group"> 
                 <button type="button" class="btn btn-info">  
                  <i class="fa fa-envelope"></i> 
                 </button> 
                 <span class="badge badge-light">5</span> 
               </span> 
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        shoppingBag: state.cart.shoppingBag
    }
}

export default connect(mapStateToProps)(ShoppingBagTracker);