import React, { Component } from 'react';
import classes from './FullProduct.css';
import styles from './FullProduct.css';
import Quantity from '../UI/Quantity/Quantity';
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions/actionTypes';
import {withRouter} from 'react-router-dom';
import SideBar from '../UI/SideBar/SideBar';

class FullProduct extends Component {

    state = {
        openSideBar: false,
        quantity: 1
    }

    componentDidMount() {

            const product = this.props.shoppingBag.find(product => product.id === this.props.selectedId);

            if(product) {
                this.setState({
                    quantity: product.quantity
                });
            }
            else {
                this.setState({
                    quantity: 1
                });
                this.props.onResetQty();
            }
        
    }
/*
    componentWillMount() {
       
        this.unlisten = this.props.history.listen((location, action) => {
            this.props.onResetQty();
        });
    }
    
    componentWillUnmount() {
          this.unlisten();
    }*/

    addToCartHandler = (id, price, title, descr) => {
        this.props.onAddToCart(id, price, title, descr);
        this.setState({openSideBar: true});
    }

    closeSideBarHandler = () => {
        this.setState({openSideBar: false});
    }
 
    render() {

        let sideBar = null;
        if(this.state.openSideBar) {
            sideBar = <SideBar close={this.closeSideBarHandler}/>
        }
        return (
            <div className={classes.FullProduct} >
                <div id={styles.image}>
                    <img src={'http://localhost:5000/api/images/'+ this.props.src} alt={this.props.alt}></img>
                </div>
                <div id={styles.product}>
                    <h1>{this.props.title}</h1>
                    <p>{this.props.description}</p>
                    <Quantity id={this.props.id} quantity={this.state.quantity} productSpec={true} />
                    <h2>{this.props.price}</h2>
                    <button onClick={() => this.addToCartHandler(this.props.selectedId, 
                                                                this.props.price, 
                                                                this.props.title,
                                                                this.props.description)}>Add to bag</button>
                </div>
                {sideBar}
            </div>
    )}
}

const mapStateToProps = state => {
    return {
        quantity: state.cart.quantity,
        selectedId: state.red.selectedId,
        shoppingBag: state.cart.shoppingBag
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onResetQty: () => dispatch({type: actionTypes.RESET_QUANTITY}),
        onAddToCart: (id, price, title, descr) => dispatch({type: actionTypes.ADD_TO_CART, id: id, price: price, 
            title: title, descr: descr})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(FullProduct));