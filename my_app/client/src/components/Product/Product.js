import React from 'react';
import classes from './Product.css';

const product = (props) => {

        return (
            <div className={classes.Product}>
                <img src={'http://localhost:5000/api/images/'+ props.src} alt={props.alt} onClick={props.selected}></img>
                <h5 >{props.title}</h5>
                <p>{props.price}</p>
                <button onClick={props.addToCart}>Add to bag</button>
            </div>
        )
    
}

export default product;


    /*
    state = {
        imageSRC: ''
    }

    componentDidMount() {

        const url = 'http://localhost:5000/api/images/'+ this.props.src;
        console.log(url)
        axios.get(url, {
             headers: {
             'Content-Type': 'multipart/form-data',
             'x-Trigger': 'CORS'
             },
         })
         .then((response) => {
             this.setState({
                 imageSRC: url
             })
         })
         .catch((err) => {
             console.log(err);
         });
    }
   */ 
 