import React, { Component } from 'react';
import classes from './NavigationBar.css';
import NavigationItems from '../NavigationItems/NavigationItems';
import Logo from '../../Logo/Logo';

class NavigationBar extends Component{
    
    render() {
        return(
            <div className={classes.Navbar}>
                <Logo />
                <nav>
                   <NavigationItems />
                </nav>
            </div>
        )
    }
    
}

export default NavigationBar;