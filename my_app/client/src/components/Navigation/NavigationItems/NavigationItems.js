import React, { Component } from 'react';
import NavigationItem from './NavigationItem/NavigationItem';
import classes from './NavigationItems.css';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as actions from '../../../store/actions/index';

class NavigationItems extends Component {

    state = {
        searchKeyword: ''
    }

    getSearchKeywordHandler = (e) => {
        this.setState({
            searchKeyword: e.target.value
        });

    }

    resetInputValue = () => {
        document.getElementById("search").reset();
    }

    submitSearchHandler = (e) => {
        if(e.key === 'Enter') {
            this.props.onSearchProducts(this.state.searchKeyword);
            e.preventDefault();
            this.resetInputValue();
            this.props.history.push('/search?' + this.state.searchKeyword);
        }
    }

    render() {

        return (
            <ul className={classes.NavigationItems}>
                    <form id="search" >
                        <input type="text" placeholder="Search" onChange={(event) => this.getSearchKeywordHandler(event)}
                        onKeyDown={(event) => this.submitSearchHandler(event)}/>
                    </form>
                    { this.props.isLoggedIn ? 
                        <NavigationItem link="/sign-out" exact activeStyle={{fontWeight: "bold"}}>sign out</NavigationItem> :
                        <NavigationItem link="/sign-in" exact activeStyle={{fontWeight: "bold"}}>sign in</NavigationItem>
                    }
                  
                <NavigationItem link="/shopping-bag" exact activeStyle={{fontWeight: "bold"}}>shopping bag {this.props.shoppingBag.length}</NavigationItem>
                    {this.props.isAdmin ? <NavigationItem link="/admin-panel" exact activeStyle={{fontWeight: "bold"}}>admin panel</NavigationItem> : null}
            </ul>
        )
    }
}

const mapStateToProps = state => {
    return {
        isLoggedIn: state.auth.isLoggedIn,
        isAdmin: state.auth.isAdmin,
        shoppingBag: state.cart.shoppingBag,
        products: state.red.products
    }
}


const mapDispatchToProps = dispatch => {
    return {
        onSearchProducts: (searchKeyword) => dispatch(actions.fetchProductSBySearchKeyword(searchKeyword))

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(NavigationItems));