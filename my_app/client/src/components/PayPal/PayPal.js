import React from 'react';
import PaypalExpressBtn from 'react-paypal-express-checkout';
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions/actionTypes';
import Alert from '../Alert/Alert';

class Paypal extends React.Component {


    onSuccess = () => {
        // Congratulation, it came here means everything's fine!
        console.log("The payment was succeeded!",);
        // You can bind the "payment" object's value to your state or props or whatever here, please see below for sample returned data
        this.props.onPaid();
    }

    onCancel = () => {
        // User pressed "cancel" or close Paypal's popup!
        console.log('The payment was cancelled!');
        // You can bind the "data" object's value to your state or props or whatever here, please see below for sample returned data
    }

    onError = () => {
        // The main Paypal's script cannot be loaded or somethings block the loading of that script!
        console.log("Error!");
        // Because the Paypal's main script is loaded asynchronously from "https://www.paypalobjects.com/api/checkout.js"
        // => sometimes it may take about 0.5 second for everything to get set, or for the button to appear
    }

    render() {

        let env = 'sandbox'; // you can set here to 'production' for production
        let currency = 'USD'; // or you can set this value from your props or state
        let total = this.props.grandTotal; // same as above, this is the total amount (based on currency) to be paid by using Paypal express checkout
        // Document on Paypal's currency code: https://developer.paypal.com/docs/classic/api/currency_codes/

        const client = {
            sandbox: 'AS02fNX6SxzO8TGP8-vAisXhrc_WD6GgNKwvze3VdSQ7DpZNl1ETL5m4dNaAD9i4_65x-rNAuAjF5jmk',
            production: 'testnodejs',
        }
        // In order to get production's app-ID, you will have to send your app to Paypal for approval first
        // For sandbox app-ID (after logging into your developer account, please locate the "REST API apps" section, click "Create App"):
        //   => https://developer.paypal.com/docs/classic/lifecycle/sb_credentials/
        // For production app-ID:
        //   => https://developer.paypal.com/docs/classic/lifecycle/goingLive/

        // NB. You can also have many Paypal express checkout buttons on page, just pass in the correct amount and they will work!
        return (
            <div>
                <PaypalExpressBtn
                    env={env}
                    client={client}
                    currency={currency}
                    total={total}
                    onError={this.onError}
                    onSuccess={this.onSuccess}
                    onCancel={this.onCancel}
                    style={{ 
                        size:'large',
                        color:'blue',
                        shape: 'rect',
                        label: 'checkout'
                    }}
                 />
                {
                    this.props.paid ? 
                        <Alert />: null
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        grandTotal: state.cart.grandTotal,
        paid: state.ord.paid
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onPaid: () => dispatch({type: actionTypes.IS_PAID})
    }   
}

export default connect(mapStateToProps, mapDispatchToProps)(Paypal);