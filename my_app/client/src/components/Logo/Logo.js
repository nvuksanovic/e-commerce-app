import React from 'react';
import logoImg from '../../assets/glossier.png';
import {NavLink} from 'react-router-dom';
import classes from './Logo.css';

const logo = (props) => {
    return (
        <div className={classes.Logo}>
            <NavLink to="/" exact><img src={logoImg} alt="logo" /></NavLink>
        </div>
    )
}

export default logo;