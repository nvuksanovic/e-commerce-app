import React, { Component } from 'react';
import classes from './Alert.css';
import styles from './Alert.css';
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions/actionTypes';
import { withRouter } from 'react-router-dom';

class Alert extends Component {

    state = {
        exit: false
    }

    okHandler = () => {
        this.props.onResetShoppingBag();
        this.props.history.push("/");
    }

    exitHandler = () => {
        this.setState({
            exit: true
        })
    }

    render() {
        return (
            <div>
                {
                    this.state.exit ? 
                        null:
                        (
                            <div className={classes.Alert}>
                                <div id={styles.dialog}>
                                    <p>Payment completed successfully, thank you!</p>
                                    <div id={styles.buttons}>
                                            <button id={styles.ok} onClick={this.okHandler}>OK</button>
                                    </div>
                                    <span id={styles.close} onClick={this.exitHandler}>x</span>
                                </div>
                            </div>   
                        )
                }
            </div>      
        )
    }
}


const mapDispatchToProps = dispatch => {
    return {
        onResetShoppingBag: () => dispatch({type: actionTypes.RESET_SHOPPING_BAG})
    }
}

export default connect(null, mapDispatchToProps)(withRouter(Alert));