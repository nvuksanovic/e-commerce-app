import React, { Component } from 'react';
import classes from './SignOut.css';
import styles from './SignOut.css';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';

class SignOut extends Component {

    exitHandler = () => {
        this.props.history.push("/");
    }

    yesHandler = () => {
        this.props.onLogOut();
        this.props.history.push("/");
    }

    render() {
        return (
            <div className={classes.SignOut}>
                <div id={styles.dialog}>
                    <p>Are you sure you want to sign out?</p>
                    <div id={styles.buttons}>
                            <button onClick={this.yesHandler}>Yes</button>
                            <button onClick={this.exitHandler}>No</button>
                    </div>
                    <span id={styles.close} onClick={this.exitHandler}>x</span>
                </div>
            </div>      
        )
    }
}

const mapStateToProps = state => {
    return {
        isLoggedIn: state.auth.isLoggedIn,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onLogOut: () => dispatch(actions.userLogOut()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignOut);