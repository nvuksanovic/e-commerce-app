import React, { Component } from 'react';
import classes from './SideBar.css';
import styles from './SideBar.css';
import { connect } from 'react-redux';
import SideBarItem from './SideBarItem/SideBarItem';
import {NavLink} from 'react-router-dom';
import * as actionTypes from '../../../store/actions/actionTypes';

class SideBar extends Component {

   
    removeItemHandler = (id) => {

        this.props.onRemoveFromCart(id);
    }
   
    render() {

        return(
            <div className={classes.SideBar}>
                <div id={styles.sideBar}>
                    <span onClick={this.props.close}>X</span>
                    <h2>Shopping Bag</h2>
                    <h3>({this.props.shoppingBag.length} items)</h3>
                    <hr></hr>
                    <ul>
                        {this.props.shoppingBag.map(item => (
                            <SideBarItem 
                                key={item.id}
                                title={item.title}
                                descr={item.descr}
                                quantity={item.quantity}
                                price={item.price}
                                remove={() => this.removeItemHandler(item.id)}
                            />
                        ))}
                    </ul>
                    <h4>Subtotal: ${this.props.totalPrice}</h4>
                    <NavLink to="/shopping-bag">View full shopping bag</NavLink>
                </div>
                <div id={styles.backdrop} />
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        shoppingBag: state.cart.shoppingBag,
        totalPrice: state.cart.totalPrice
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onRemoveFromCart: (id) => dispatch({type: actionTypes.REMOVE_FROM_CART, id: id})
    } 
}

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);