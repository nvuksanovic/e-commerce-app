import React from 'react';
import classes from './SideBarItem.css';
import ReactTruncatedComponent from 'react-truncated-component';

const sideBarItem = (props) => {

    return (
        <div className={classes.SideBarItem}>
            <h2>{props.title}</h2>
            <ReactTruncatedComponent ellipsis="..." numberOfLines={3} lineHeight={23}>
                <p>{props.descr}</p>
            </ReactTruncatedComponent >
            <h3>{props.price} X {props.quantity}</h3>
            <h4 onClick={props.remove}>Remove</h4>
            <hr />
        </div>
    )
}

export default sideBarItem;