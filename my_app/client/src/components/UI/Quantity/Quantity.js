import React, { Component } from 'react';
import classes from './Quantity.css';
import * as actionTypes from '../../../store/actions/actionTypes';
import { connect } from 'react-redux';

class Quantity extends Component {

    state = {
        disabled: false
    }

    addItemHandler = (id) => {
        this.props.onIncrement(id);
    }

    deleteItemHandler = (id) => {
        this.props.onDecrement(id);
    }

    render() {

        return (
            <div className={classes.Quantity}>
                <button onClick={() => this.addItemHandler(this.props.id)}>+</button>
                <input type="number" value={ !this.props.productSpec ? 
                    this.props.quantity : 
                    this.props.value } readOnly/>
                <button onClick={() => this.deleteItemHandler(this.props.id)} disabled={this.props.quantity <= 1 && this.props.value <= 1}>-</button>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        value: state.cart.quantity,
        selectedId: state.red.selectedId,
        shoppingBag: state.cart.shoppingBag,
        products: state.red.products
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onIncrement: (id) => dispatch({type: actionTypes.INCREMENT_QUANTITY, id:id}),
        onDecrement: (id) => dispatch({type: actionTypes.DECREMENT_QUANTITY, id:id}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Quantity);

