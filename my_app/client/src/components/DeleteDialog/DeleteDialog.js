import React, { Component } from 'react';
import classes from './DeleteDialog.css';
import styles from './DeleteDialog.css';
import { withRouter } from 'react-router-dom';


class DeleteDialog extends Component {

    exitHandler = () => {
        this.props.history.push("/");
    }

    render() {
        return (

            <div className={classes.DeleteDialog}>
                <div id={styles.dialog}>
                <p>You have successfully deleted product!</p>
                <span id={styles.close} onClick={this.exitHandler}>x</span>
                </div>
            </div>   

        )
    }
}


export default withRouter(DeleteDialog);