const express = require('express');
const router = express.Router();
const Cart = require('../models/Cart');
const verify = require('../validations/verifyToken');


router.post('/', verify,  async (req, res) => {

    const userId = req.user._id;
  
    try {

        const newCart = new Cart ({
            userId,
            products: req.body.orderItems,
            totalPrice: req.body.totalPrice
        })

        const savedCart = await newCart.save();
        return res.status(201).send(savedCart._id);
        
        
    } catch (error) {
        res.status(500).send(error);
    }
})

router.get('/:token', verify,  async (req, res) => {

    const userId = req.user._id;
  
    Cart.findOne({userId: userId}, (err, cart) => {
        if (err) {
            console.log(err);
          } else {
            console.log(cart)
            res.send(cart);
          }
    })
})

module.exports = router;