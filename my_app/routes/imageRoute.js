const express = require('express');
const router = express.Router();
const Grid = require("gridfs-stream");
const mongoose = require('mongoose');
const crypto = require("crypto");


module.exports = (upload) => {

    const mongoURI = process.env.DATA_BASE_CONNECT;

    const conn = mongoose.createConnection(mongoURI, { useUnifiedTopology: true, useNewUrlParser: true });


    //GridFS config

    let gfs;

    conn.once("open", () => {
        gfs = Grid(conn.db, mongoose.mongo);
        gfs.collection("uploads");
        console.log("Connection Successful");
    });

 

    router.post("/upload", upload.single("image"), (req, res, next) => {
        res.send(req.files);

    });


    router.get("/:filename", (req, res) => {    

      gfs.files.findOne({ filename: req.params.filename }, (err, file) => {
          if (!file || file.length === 0) {
            return res.status(404).json({
              err: "No file exists"
            });
          }
      
          if (file.contentType === "image/jpeg" || file.contentType === "image/png") {
            const readstream = gfs.createReadStream(file.filename);
            readstream.pipe(res);
          } else {
            res.status(200).json({
                success: true,
                file: files[0]
            });
          }
        });
        
 }); 

    return router;
}