const stripe = require('stripe')("sk_test_51HSsZCILfzI5UxegRRv4bQOAKWBHwHO5joPPXhJgVqDxGhNGR5Y8YTrgydobrPDT5zO1zk9FoJmDA6Il2k05Lp2r00QS5BRHRs");
const { v4: uuidv4 } = require('uuid');
const express = require('express');
const router = express.Router();
const paypal = require('paypal-rest-sdk');  
const Cart = require('../models/Cart');
const verify = require('../validations/verifyToken');


//CREDIT CARD - STRIPE

router.post('/', verify, (req, res) => {

    const {token, totalPrice} = req.body;
    
    try {
        let data = stripe.charges.create(
            { 
               amount: totalPrice * 100,     
               description: 'Web Development Product', 
               currency: 'usd', 
               source: token.id
           }
       )

       console.log(data);
       res.send("Charged!")
        
    } catch (error) {
        console.log(error);
        res.status(500);
    }

})

//PAYPAL
/*
paypal.configure({
    'mode': 'sandbox', //sandbox or live
    'client_id': 'AS02fNX6SxzO8TGP8-vAisXhrc_WD6GgNKwvze3VdSQ7DpZNl1ETL5m4dNaAD9i4_65x-rNAuAjF5jmk',
    'client_secret': 'EIBI9Au-VLD3XO_Jlqw-Rzxdw-PCFV4WHAmnUgtASh8SlfDQyHxsA82Brpkr4e6xyAiyEOlI3YNNfSj_'
});

router.post('/paypal', async (req, res) => {

    const {cartId} = req.body;
    console.log("USER ID ", cartId)
    const cart = await Cart.findById(cartId);
    console.log("CART ", cart)
    if(!cart) {
        return res.status(400).send("Cart doesn't exist!");
    }

    const create_payment_json = {
      "intent": "sale",
      "payer": {
          "payment_method": "paypal"
      },
      "redirect_urls": {
          "return_url": "http://localhost:3000/success",
          "cancel_url": "http://localhost:3000/cancel"
      },
      "transactions": [{
          "item_list": {
              "items": cart.products
          },
          "amount": {
              "currency": "USD",
              "total": cart.totalPrice
          },
          "description": "Hat for the best team ever"
      }]
  };

  paypal.payment.create(create_payment_json, function (error, payment) {
    if (error) {
        throw error;
    } else {
        for(let i = 0;i < payment.links.length;i++){
          if(payment.links[i].rel === 'approval_url'){
            res.redirect(payment.links[i].href);
          }
        }
    }
  });
  
  });
  
  router.get('/success', (req, res) => {
    const payerId = req.query.PayerID;
    const paymentId = req.query.paymentId;
  
    const execute_payment_json = {
      "payer_id": payerId,
      "transactions": [{
          "amount": {
              "currency": "USD",
              "total": "25.00"
          }
      }]
    };
  
    paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
      if (error) {
          console.log(error.response);
          throw error;
      } else {
          console.log(JSON.stringify(payment));
          res.send('Success');
      }
  });
  });
 
*/

module.exports = router;
