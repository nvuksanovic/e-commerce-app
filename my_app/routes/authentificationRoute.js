const Joi = require('@hapi/joi');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const router = require('express').Router();
const User = require('../models/User');
const verify = require('../validations/verifyToken');
const { registerValidation, loginValidation } = require('../validations/joiValidation');

//REGISTER 
router.post('/register', async (req, res) => {

    //LET'S VALIDATE DATA

    const { error } = registerValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    
    //Check if user already exists in db
    const emailExist = await User.findOne({email: req.body.email});
    if(emailExist) return res.status(400).send('Email already exists');

    //Hash the password

    const salt = await bcrypt.genSalt(10); 
    const hashedPassword = await bcrypt.hash(req.body.password, salt);
       
    try {

         //Create new user
        const user = new User({
            name: req.body.name,
            email: req.body.email,
            password: hashedPassword,
            lastName: req.body.lastName,
            address: req.body.address,
            city: req.body.city,
            zip: req.body.zip,
            country: req.body.country,
            isAdmin: false
        });

        const savedUser = await user.save();

        const token = jwt.sign({_id: user._id}, process.env.TOKEN_SECRET, { expiresIn: "1h"});

        res.header('auth-token', token);
        res.send({
            _id: user.id,
            name: user.name,
            email: user.email,
            isAdmin: user.isAdmin,
            token: token
        });
        
    } catch (error) {
        res.status(400).send({message: 'Invalid User Data'});
    }
});

//LOGIN
router.post('/login', async (req, res) => {

    //LET'S VALIDATE DATA
    const { error } = loginValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    //Check if user already exists in db
    const user = await User.findOne({email: req.body.email});
    if(!user) return res.status(400).send('Email is not found');

    //Check if password is correct
    const validPassword = await bcrypt.compare(req.body.password, user.password); 
    if(!validPassword) return res.status(400).send('Invalid password');
    
    //Create and assign a token
    const token = jwt.sign({_id: user._id}, process.env.TOKEN_SECRET, { expiresIn: "1h"});

    res.header('auth-token', token);
    res.send({
        _id: user.id,
        name: user.name,
        email: user.email,
        isAdmin: user.isAdmin,
        token: token
    });
});

//CREATE ADMIN

router.get('/createadmin', async (req, res) => {
    try {
        const salt = await bcrypt.genSalt(10); 
        const hashedPassword = await bcrypt.hash("123456", salt);
        const user = new User({
            name: 'Natasa',
            email: 'admin@example.com',
            password: hashedPassword,
            isAdmin: true,
            lastName: 'Vuksanovic',
            address: 'Address Example 123',
            city: 'Belgrade',
            zip: 11000,
            country: 'Serbia'
        });
        const newUser = await user.save();
        res.send(newUser);
    } catch (error) {
      res.send({ message: error.message });
    }
  });
  
  router.get('/:token', verify, async (req, res) => {
    
        const userId = req.user._id;
        
        User.findById(userId, (err, user) => {
            if (err) {
                console.log(err);
              } else {
                res.send(user);
              }
        })
  })


module.exports = router;