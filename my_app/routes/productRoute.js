const router = require('express').Router();
const Product = require('../models/Product');
const User = require('../models/User');
const verify = require('../validations/verifyToken');

router.post('/', verify, async (req, res) => {

    const user = await User.findById(req.user._id);

    if (req.user && user.isAdmin) {
        const product = new Product({
            title: req.body.title,
            description: req.body.description,
            price: req.body.price,
            image: req.body.image
        })
        try {
            const savedProduct = await product.save();
            res.send({product: product._id});
        } catch (error) {
            res.status(400).send({message: 'Invalid Product Data'});
        }
    } else {
        return res.status(401).send({ message: 'Admin Token is not valid.' });
    }

});


router.get('/data', (req, res) => {

    Product.find({}, function(err, result) {
        if (err) {
          console.log(err);
        } else {
          res.send(result);
        }
    })
    
});

router.delete('/:title', verify, async (req, res) => {

     const user = await User.findById(req.user._id);

    if (req.user && user.isAdmin) {
        //const deletedProduct = await Product.findById(req.params.id);
        const deletedProduct = await Product.findOne({title: req.params.title});
        console.log(deletedProduct)
        if (deletedProduct) {
        await deletedProduct.remove();
        res.send({ message: 'Product Deleted' });
        } else {
        res.send('Error in Deletion.');
        }
    } else {
        return res.status(401).send({ message: 'Admin Token is not valid.' });
    }
    
  });

  router.put('/update/:id', verify, async (req, res) => {

    const user = await User.findById(req.user._id);

   if (req.user && user.isAdmin) {

        const productId = { _id: req.params.id };
        
        const product = await Product.findById(productId);
        if (product) {
            
            product.title = req.body.title,
            product.description = req.body.description,
            product.price = req.body.price,
            product.image = req.body.image
            
            const updatedProduct = await product.save();
            
            if (updatedProduct) {
              return res.status(200).send({ message: 'Product Updated', data: updatedProduct });
            }
        }

        else {
            return res.status(401).send({ message: 'Can\'t find product!' });
        }

   } else {
       return res.status(401).send({ message: 'Admin Token is not valid.' });
   }
   
 });

  router.get('/search', (req, res) => {
    
    const searchKeyword = req.query.searchKeyword;
    let regex = new RegExp(".*" + searchKeyword + ".*");

    if(searchKeyword) {
        Product.find({title: regex}, function(err, result) {
            if (err) {
              console.log(err);
            } else {
              res.send(result);
            }
        })
    }
    else {
        res.send("No search keyword!");
    }
  })
  

module.exports = router;